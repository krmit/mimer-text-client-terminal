# Mimer text client for the terminal

This is a simple terminal base client for the text protocol. The program must
be run in a terminal with support for terminal colors.

## Install

```
$ npm set registry http://htsit.se:4873
$ npm install
```

## Usage

Connecting to a test server.

```
tct http://htsit.se/3040
```

## License

GPL version 3, see the file COPYING.

## Contribute

Please feel free to open an issue or submit a pull request.

## Changelog

- **0.1.0** _2022-07-08_ First version published

## Author

**©Magnus Kronnäs 2022 [magnus.kronnas.se](https://magnus.kronnas.se)**