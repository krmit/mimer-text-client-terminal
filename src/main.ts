#!/usr/bin/env node
import { program } from "commander";
import fetch from "node-fetch";
import { TextData, Answer } from "@mimer/text";
import { doTerminal, textToTerminal, Options } from "./textToTerminal";

async function main() {
  program
    .name("tct")
    .description("A client for the mimer-text protocol")
    .version("0.0.1")
    .option("-d, --debug", "Print server respons as json.")
    .option("-t, --trace", "Print the array generatet from the text object.")
    .option("-s, --silent", "Will not print terminal text.")
    .option("-v, --verbose", "Print extra information messages.")
    .argument(
      "[path]",
      "url to server or path to file",
      "http://127.0.0.1:8080"
    );
  program.parse();
  const opts = program.opts();
  const args = program.args;

  const debug = true;
  const options: Options = {
    status: "next",
    debug: opts.debug ?? false,
    trace: opts.trace ?? false,
    silent: opts.silent ?? false,
    verbose: opts.verbose ?? false,
  };

  const path = args[0] ?? "http://127.0.0.1:8080";
  try {
    // Connecting to server
    const url = new URL(path);

    const first_res = await fetch(url.toString()).catch(error_fetch(url));
    let cookies = first_res.headers.raw()["set-cookie"];
    let token: string = "";
    if (cookies !== undefined && cookies.length > 0) {
      token = cookies[0].split(";")[0].split("=")[1];
    } else {
      if (options.verbose) {
        console.warn("This server do not use token!");
      }
    }

    // Security problem, should be checket
    let text = (await first_res.json()) as TextData;
    if (options.debug) {
      console.log(JSON.stringify(text, null, 4));
    }
    let output = textToTerminal(text);
    if (options.trace) {
      console.log(output);
    }

    let answer = {};
    await doTerminal(output, answer, options);
    while (options.status !== "stop") {
      url.pathname = text.next ?? url.pathname;
      const option: any = {
        headers: {
          cookie: `token=${token}`,
        },
        method: "GET",
      };
      if (options.status === "question") {
        option.headers["Content-Type"] = "application/json";
        option.method = "POST";
        option.body = JSON.stringify(answer);
      }
      let res = await fetch(url.toString(), option).catch(error_fetch(url));

      // Security problem, should be checket
      text = (await res.json()) as TextData;
      if (options.debug) {
        console.log(JSON.stringify(text, null, 4));
      }
      let output = textToTerminal(text);
      if (options.trace) {
        console.log(output);
      }
      answer = {};
      await doTerminal(output, answer, options);
    }
  } catch (err) {
    console.error("Stop program because of errors.");
    console.error(err);
  }
}

main();

function error_fetch(url: URL) {
  return () => {
    console.error(`Can not connect to servera at ${url.toString()}`);
    throw "";
  };
}
